import csv

from src.items.models import Item


def run():
    with open('/Users/omar/Downloads/radical_trad.csv', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)

        number = 1
        for row in csv_reader:
            item = Item(
                name=row['name'],
                character=row['character'],
                story=row['story'],
                number=number
            )
            item.save()
            number += 1

    with open('/Users/omar/Downloads/kanji_trad.csv', mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)

        number = 1
        for row in csv_reader:
            item = Item(
                name=row['name'],
                character=row['character'],
                story=row['story'],
                number=number
            )
            item.save()
            number += 1
