FROM python:3.7

RUN apt-get update -y && apt-get upgrade -y
RUN mkdir /app
WORKDIR /app

RUN pip install pipenv
COPY Pipfile* /tmp/
RUN cd /tmp && pipenv lock --requirements > requirements.txt
RUN pip install -r /tmp/requirements.txt

COPY . /app/

EXPOSE 8000

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

CMD python manage.py makemigrations
CMD python manage.py migrate
CMD gunicorn settings.wsgi:application
