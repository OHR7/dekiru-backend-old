from django.contrib import admin

from src.items.models import Deck, Item, ItemReview

admin.site.register(Item)
admin.site.register(ItemReview)
admin.site.register(Deck)
