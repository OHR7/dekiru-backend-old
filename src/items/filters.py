from url_filter.filtersets import ModelFilterSet

from src.items.models import Item, ItemReview, Deck


class ItemFilterSet(ModelFilterSet):
    class Meta(object):
        model = Item


class ItemReviewFilterSet(ModelFilterSet):
    class Meta(object):
        model = ItemReview


class DeckFilterSet(ModelFilterSet):
    class Meta(object):
        model = Deck
