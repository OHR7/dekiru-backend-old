from rest_framework import serializers

from src.items.models import ItemReview, Item, Deck


class ItemSerializer(serializers.ModelSerializer):
    components = serializers.PrimaryKeyRelatedField(many=True, queryset=Item.objects)

    class Meta:
        model = Item
        fields = '__all__'


class ItemReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = ItemReview
        fields = '__all__'


class DeckSerializer(serializers.ModelSerializer):
    items = serializers.PrimaryKeyRelatedField(many=True, queryset=Item.objects)
    item_reviews = serializers.PrimaryKeyRelatedField(many=True, queryset=ItemReview.objects)

    class Meta:
        model = Deck
        fields = '__all__'


class DeckNameSerializer(serializers.ModelSerializer):
    class Meta:
        model = Deck
        fields = ['name']
