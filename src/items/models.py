from django.db import models


class Item(models.Model):
    name = models.CharField(max_length=100)
    character = models.CharField(max_length=120, null=True)
    story = models.TextField(max_length=300)
    number = models.IntegerField()
    components = models.ManyToManyField('Item', related_name='items', blank=True)


class ItemReview(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    item = models.ForeignKey('Item', on_delete=models.CASCADE)
    level = models.ForeignKey('reviews.ReviewLevel', on_delete=models.CASCADE)
    date = models.DateTimeField()
    deck = models.ForeignKey('Deck', on_delete=models.CASCADE)
    learning = models.BooleanField(default=True)


class Deck(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    items = models.ManyToManyField('Item', related_name='deck', blank=True)
