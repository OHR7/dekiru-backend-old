from datetime import timedelta, datetime, timezone

from rest_framework import viewsets
from rest_framework.decorators import api_view
from rest_framework.response import Response

from src.items.filters import ItemFilterSet, ItemReviewFilterSet, DeckFilterSet
from src.items.models import Deck, Item, ItemReview
from src.items.serializers import ItemSerializer, DeckSerializer, ItemReviewSerializer
from src.reviews.models import ReviewLevel, Review, ReviewSummary


class ItemViewSet(viewsets.ModelViewSet):
    """
    A View Set for viewing and editing items instances.
    """
    serializer_class = ItemSerializer
    queryset = Item.objects.all()
    filter_class = ItemFilterSet


class ItemReviewViewSet(viewsets.ModelViewSet):
    """
    A View Set for viewing and editing items review instances.
    """
    serializer_class = ItemReviewSerializer
    queryset = ItemReview.objects.all().order_by('-item__number')
    filter_class = ItemReviewFilterSet


class DeckViewSet(viewsets.ModelViewSet):
    """
    A View Set for viewing and editing decks instances.
    """
    serializer_class = DeckSerializer
    queryset = Deck.objects.all()
    filter_class = DeckFilterSet


@api_view(['PATCH'])
def update_item(request, item_id):
    """
    {
        deck_id: int,
        answer: bool,
    }
    """
    item_review, created = ItemReview.objects.get_or_create(
        user_id=1,
        item_id=item_id,
        deck_id=request.data['deck_id'],
        defaults={
            'level_id': 1,
            'date': datetime.now(tz=timezone.utc) + timedelta(days=1),
        }
    )
    deck = Deck.objects.get(pk=request.data['deck_id'])
    review = Review.objects.get(user_id=1, deck=deck)
    item = Item.objects.get(pk=item_id)

    if request.data['answer']:
        if not created:
            review.items.remove(item)
            next_level = ReviewLevel.objects.get(pk=item_review.level_id + 1)
        else:
            review.new_items.remove(item)
            next_level = ReviewLevel.objects.get(pk=item_review.level_id)

        item_review.level = next_level
        item_review.date = datetime.now(tz=timezone.utc) + timedelta(days=next_level.interval)
        item_review.learning = False

    else:
        if not item_review.learning:
            if item_review.level_id >= 4:
                # TODO Change 2 for user level preference
                next_level = ReviewLevel.objects.get(pk=item_review.level_id - 2)
            else:
                next_level = ReviewLevel.objects.get(pk=1)
            item_review.level = next_level
            item_review.date = datetime.now(tz=timezone.utc) + timedelta(days=next_level.interval)
            item_review.learning = True
    item_review.save()

    review_summary, created = ReviewSummary.objects.get_or_create(
        user_id=1,
        deck=deck,
        date=datetime.utcnow(),
        defaults={
            'score': 0,
            'completed': False,
        }
    )

    if not review.items.all().exists() and not review.new_items.all().exists():
        review_summary.completed = True
        review_summary.save()
    review_summary.items.add(item)

    return Response()
