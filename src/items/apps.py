from django.apps import AppConfig


class ItemsConfig(AppConfig):
    name = 'src.items'
