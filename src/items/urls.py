from django.urls import path, include
from rest_framework.routers import DefaultRouter

from src.items.views import ItemViewSet, DeckViewSet, ItemReviewViewSet, update_item

router = DefaultRouter()
router.register(r'decks', DeckViewSet, basename='deck')
router.register(r'reviews', ItemReviewViewSet, basename='item-review')
router.register(r'', ItemViewSet, basename='item')

urlpatterns = [
    path('update/<int:item_id>', update_item, name='update-item-review'),
    path(r'', include(router.urls)),
]
