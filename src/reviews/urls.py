from django.urls import path, include
from rest_framework.routers import DefaultRouter

from src.reviews.views import ReviewSummaryViewSet, ReviewViewSet, ReviewLevelViewSet, get_today_reviews, get_calendar

router = DefaultRouter()
router.register(r'summary', ReviewSummaryViewSet, basename='review-summary')
router.register(r'level', ReviewLevelViewSet, basename='review-level')
router.register(r'', ReviewViewSet, basename='review')

urlpatterns = [
    path('today/<int:deck_id>', get_today_reviews, name='today-reviews'),
    path('calendar', get_calendar, name='reviews-calendar'),
    path(r'', include(router.urls)),
]
