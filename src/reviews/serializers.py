from rest_framework import serializers

from src.items.models import Item
from src.items.serializers import DeckNameSerializer
from src.reviews.models import Review, ReviewSummary, ReviewLevel


class ReviewSerializer(serializers.ModelSerializer):
    items = serializers.PrimaryKeyRelatedField(many=True, queryset=Item.objects)
    new_items = serializers.PrimaryKeyRelatedField(many=True, queryset=Item.objects)

    class Meta:
        model = Review
        fields = '__all__'


class ReviewSummarySerializer(serializers.ModelSerializer):
    items = serializers.PrimaryKeyRelatedField(many=True, queryset=Item.objects)

    class Meta:
        model = ReviewSummary
        fields = '__all__'


class ReviewLevelSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReviewLevel
        fields = '__all__'
