from url_filter.filtersets import ModelFilterSet

from src.reviews.models import Review, ReviewSummary, ReviewLevel


class ReviewFilterSet(ModelFilterSet):
    class Meta(object):
        model = Review


class ReviewSummaryFilterSet(ModelFilterSet):
    class Meta(object):
        model = ReviewSummary


class ReviewLevelFilterSet(ModelFilterSet):
    class Meta(object):
        model = ReviewLevel
