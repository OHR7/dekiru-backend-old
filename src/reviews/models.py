from django.db import models


class Review(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    items = models.ManyToManyField('items.Item', related_name='item_reviews', blank=True)
    new_items = models.ManyToManyField('items.Item', related_name='new_item_reviews', blank=True)
    deck = models.ForeignKey('items.Deck', on_delete=models.CASCADE)
    fetched_date = models.DateField(null=True, blank=True)


class ReviewSummary(models.Model):
    user = models.ForeignKey('users.User', on_delete=models.CASCADE)
    items = models.ManyToManyField('items.Item', related_name='review_summary', blank=True)
    date = models.DateField()
    score = models.IntegerField(default=0)
    completed = models.BooleanField(default=False)
    deck = models.ForeignKey('items.Deck', on_delete=models.CASCADE)


class ReviewLevel(models.Model):
    interval = models.IntegerField()
    stage = models.CharField(max_length=150)
