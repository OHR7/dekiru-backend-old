from django.contrib import admin

from src.reviews.models import Review, ReviewSummary, ReviewLevel

admin.site.register(Review)
admin.site.register(ReviewSummary)
admin.site.register(ReviewLevel)
