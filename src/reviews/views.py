from datetime import datetime, timedelta, date, timezone

from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from src.items.models import Deck, ItemReview
from src.reviews.filters import ReviewLevelFilterSet, ReviewSummaryFilterSet, ReviewFilterSet
from src.reviews.models import Review, ReviewSummary, ReviewLevel
from src.reviews.serializers import ReviewSerializer, ReviewSummarySerializer, ReviewLevelSerializer


class ReviewViewSet(viewsets.ModelViewSet):
    """
    A View Set for viewing and editing review instances.
    """
    serializer_class = ReviewSerializer
    queryset = Review.objects.all()
    filter_class = ReviewFilterSet


class ReviewSummaryViewSet(viewsets.ModelViewSet):
    """
    A View Set for viewing and editing review summary instances.
    """
    serializer_class = ReviewSummarySerializer
    queryset = ReviewSummary.objects.all()
    filter_class = ReviewSummaryFilterSet


class ReviewLevelViewSet(viewsets.ModelViewSet):
    """
    A View Set for viewing and editing review level instances.
    """
    serializer_class = ReviewLevelSerializer
    queryset = ReviewLevel.objects.all()
    filter_class = ReviewLevelFilterSet


@api_view(['GET'])
def get_today_reviews(request, deck_id):
    deck = Deck.objects.get(pk=deck_id)
    # Get or Create Review Summary to see if user already fetch today new items
    review = Review.objects.get(user_id=1, deck=deck)
    new_item_list = review.new_items.values_list('id', flat=True)
    today = datetime.now(tz=timezone.utc).date()
    if review.fetched_date != today:
        last_reviewed_item = ItemReview.objects.filter(
            user_id=1,
            deck=deck
        ).order_by('-item__number').first()
        if last_reviewed_item:
            last_reviewed_item_number = last_reviewed_item.item.number + 1
        else:
            last_reviewed_item_number = 1

        new_items = deck.items.filter(
            number__range=(last_reviewed_item_number, last_reviewed_item_number + request.user.new_items),
        )
        new_item_list = new_items.values_list('id', flat=True)
        review.fetched_date = today
        review.save(update_fields=['fetched_date'])

    # Get Today Items Regardless if new item are required
    today_items = ItemReview.objects.filter(
        user_id=1,
        date__lte=datetime.now(tz=timezone.utc)
    ).order_by('date')
    today_items_list = today_items.values_list('item_id', flat=True)

    data = {
        'items': today_items_list,
        'new_items': new_item_list,
        'user': review.user.id,
        'deck': review.deck.id,
        'fetched_date': review.fetched_date
    }

    review_serializer = ReviewSerializer(review, data=data)
    review_serializer.is_valid()
    review_serializer.save()

    return Response(review_serializer.data)


@api_view(['GET'])
@permission_classes([IsAuthenticated])
def get_calendar(request):
    selected_date = date.today()
    studied_days = ReviewSummary.objects.filter(
        date__year=selected_date.year,
        date__month=selected_date.month,
        user_id=1
    )
    day_list = []
    for studied_day in studied_days:
        day_list.append(studied_day.date)
    print(day_list)
    summary_serializer = ReviewSummarySerializer(studied_days, many=True)
    return Response(day_list)
