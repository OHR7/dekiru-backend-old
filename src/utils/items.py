from django.db.models import Q
from django.utils.datastructures import MultiValueDictKeyError

from src.items.models import Deck
from src.users.models import User


def get_deck_name_list(request):
    try:
        deck_name_str = request.GET["deck_name"]
    except MultiValueDictKeyError as e:
        deck_name_str = None
    deck_name_list = []
    if deck_name_str:
        deck_name_list.append(deck_name_str)
    else:
        user = User.objects.get(pk=1)
        decks = Deck.objects.filter(
            Q(user_id=1) | Q(user_id=2)
        )
        for deck in decks:
            deck_name_list.append(deck.name)
    return deck_name_list
