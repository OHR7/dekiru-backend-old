from datetime import datetime, timedelta

from src.items.models import ItemReview, Deck, Item
from src.reviews.models import ReviewLevel


def get_item_lists(deck_id, user_id):
    # Get New Items
    deck = Deck.objects.get(pk=deck_id)
    last_reviewed_item = deck.item_reviews.filter(
        user_id=user_id
    ).order_by('-date').first()

    if last_reviewed_item:
        last_reviewed_item_number = last_reviewed_item.item.number + 1
    else:
        last_reviewed_item_number = 1
    new_items = deck.items.filter(
        number__range=(last_reviewed_item_number, last_reviewed_item_number + 20),
    )
    new_item_list = new_items.values_list('id', flat=True)

    # Get Today Items
    today_items = deck.item_reviews.filter(
        user_id=1,
        date__lte=datetime.today()
    ).order_by('date')
    today_items_list = today_items.values_list('item_id', flat=True)

    return new_item_list, today_items_list


def update_item_level(item_pk, user_id, params, deck):
    item_review, created = ItemReview.objects.get_or_create(
        user_id=user_id,
        item_id=item_pk,
        defaults={
            'level_id': 1,
            'date': datetime.today() + timedelta(days=1),
        }
    )

    if not created:
        if params['answer']:
            next_level = ReviewLevel.objects.get(pk=item_review.level_id + 1)
            item_review.level = next_level
            item_review.date = datetime.today() + timedelta(days=next_level.interval)
        else:
            if item_review.level_id >= 4:
                # TODO Change 2 for user level preference
                next_level = ReviewLevel.objects.get(pk=item_review.level_id - 2)
            else:
                next_level = ReviewLevel.objects.get(pk=1)
            item_review.level = next_level
            item_review.date = datetime.today() + timedelta(days=next_level.interval)
        item_review.save()
    else:
        deck.item_reviews.add(item_review)
    return item_review
