from django.contrib.auth.models import AbstractUser
from django.db import models


class User(AbstractUser):
    photo = models.CharField(default='bamboo', max_length=120)
    recovery = models.BooleanField(null=True, blank=True)
    new_items = models.IntegerField(default=20)
    subscription_id = models.CharField(default='', max_length=120)
