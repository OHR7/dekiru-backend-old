from url_filter.filtersets import ModelFilterSet

from src.users.models import User, UserVacation


class UserFilterSet(ModelFilterSet):
    class Meta(object):
        model = User


class UserVacationFilterSet(ModelFilterSet):
    class Meta(object):
        model = UserVacation
