import json
import logging

import stripe
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import viewsets
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from src.users.models import User
from src.users.serializers import UserSerializer

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class UserViewSet(viewsets.ModelViewSet):
    """
    A View Set for viewing and editing user instances.
    """
    serializer_class = UserSerializer
    queryset = User.objects.all()


@api_view(['POST'])
def user_verification(request, uid, token):
    print(uid)
    print(token)

    return Response()


@api_view(['GET'])
def user_me(request):
    logger.info(request.headers.get('Authorization'))
    serializer = UserSerializer(request.user)
    stripe.api_key = "sk_test_n7w7er4bSLSHj6s4CXWGTwlU000JP0VUhK"
    sub = stripe.Subscription.retrieve(request.user.subscription_id)
    logger.info(sub['status'])
    return Response(serializer.data)


@api_view(['DELETE'])
def cancel_subscription(request):
    print(request.headers.get('Authorization'))
    stripe.api_key = "sk_test_n7w7er4bSLSHj6s4CXWGTwlU000JP0VUhK"
    try:
        sub = stripe.Subscription.delete(request.user.subscription_id)
        print(sub['status'])
    except Exception as err:
        print(err)
    user = User.objects.get(pk=request.user.id)
    user.is_active = False
    user.save()
    return Response()


@csrf_exempt
def my_webhook_view(request):
    payload = request.body

    try:
        event = stripe.Event.construct_from(
            json.loads(payload), stripe.api_key
        )
    except ValueError as e:
        # Invalid payload
        return HttpResponse(status=400)

    # Handle the event
    if event.type == 'payment_intent.succeeded':
        payment_intent = event.data.object
        print(payment_intent)
        # contains a stripe.PaymentIntent
        # Then define and call a method to handle the successful payment intent.
        # handle_payment_intent_succeeded(payment_intent)
    elif event.type == 'payment_method.attached':
        payment_method = event.data.object
        print(payment_method)
        # contains a stripe.PaymentMethod
        # Then define and call a method to handle the successful attachment of a PaymentMethod.
        # handle_payment_method_attached(payment_method)
        # ... handle other event types
    elif event['type'] == 'checkout.session.completed':
        session = event['data']['object']['subscription']
        print(event['data']['object'])
        print(session)
        stripe.api_key = "sk_test_n7w7er4bSLSHj6s4CXWGTwlU000JP0VUhK"
        customer = stripe.Customer.retrieve(event['data']['object']['customer'])
        user = User.objects.get(email=customer['email'])
        user.subscription_id = session
        user.is_active = True
        user.save()
    elif event.type == 'payment_method.':
        pass
    else:
        # Unexpected event type
        return HttpResponse(status=400)

    return HttpResponse(status=200)
