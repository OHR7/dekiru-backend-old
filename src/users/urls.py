from django.conf.urls import url
from django.urls import path, include
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter

from src.users.views import UserViewSet, user_me, my_webhook_view, cancel_subscription

router = DefaultRouter()
router.register(r'', UserViewSet, basename='user')

urlpatterns = [
    path('me', user_me, name='user-me'),
    path('webhook', my_webhook_view, name='webhook'),
    path('subscriptions/cancel', cancel_subscription, name='cancel-subscription'),
    url(r'^auth/', obtain_auth_token),
    path(r'', include(router.urls)),
]
