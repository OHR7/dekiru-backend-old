from rest_framework import serializers
from rest_framework.fields import CharField

from src.users.models import User


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'id',
            'username',
            'first_name',
            'last_name',
            'email',
            'photo',
            'new_items'
        ]

    def create(self, validated_data):
        validated_data.pop('_password', None)
        print(validated_data)
        return User.objects.create_user(
            **validated_data
        )
